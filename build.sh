#!/bin/bash
#  __  __                     __ __        _______ _______ 
# |  |/  |.---.-.--.--.---.-.|  |__|.----.|       |     __|
# |     < |  _  |  |  |  _  ||  |  ||   _||   -   |__     |
# |__|\__||___._|\___/|___._||__|__||__|  |_______|_______|
#
#  Program :	Make KavalirOS XFCE ISO v1
#  Arch    :	x86_64 
#  Author  : 	Roelof Ridderman
#

function banner() {
	term_cols=$(tput cols) 
	str=":: $1 ::"
	for ((i=1; i<=`tput cols`; i++)); do echo -n ‾; done
	tput setaf 10; echo "$str";  tput sgr0
	for ((i=1; i<=`tput cols`; i++)); do echo -n _; done
}

banner "Make new KavalirOS GNOME ISO"

# Settings
PROFILE=gnome
WORKDIR=/var/build/work
PROFILEWORKDIR=/var/build/profile
PROFILEDIR=/usr/share/archiso/configs/releng
CALAMARESDIR=/home/roelof/Repos/kavaliros-calamares
ISODIR=/data/distro
REPODIR=/data/nas/roelof/KavalirOS/Repo

# Create work folder
banner "Clear Work folder"
if [ -d $PROFILEWORKDIR ]; then
	sudo rm -Rf $PROFILEWORKDIR
fi
if [ -d $WORKDIR ]; then
	sudo rm -Rf $WORKDIR
fi
mkdir $PROFILEWORKDIR
mkdir $WORKDIR

banner "Update system"
sudo pacman -Syy
sudo pacman -Syu

banner "Create configuration"
sudo rsync -avzh $PROFILEDIR/ $PROFILEWORKDIR/
sudo chown roelof:users $PROFILEWORKDIR/packages.x86_64
sudo cat ./$PROFILE-packages.x86_64 >> $PROFILEWORKDIR/packages.x86_64
sudo rsync -avzh --exclude '.git/*' --exclude '.git*' --exclude "$PROFILE-packages.x86_64" ./ $PROFILEWORKDIR/
sudo rsync -avzh --exclude '.git/*' --exclude '.git*' --exclude 'README.md' $CALAMARESDIR/ $PROFILEWORKDIR/airootfs/
sudo rsync -avzh $REPODIR/ $PROFILEWORKDIR/airootfs/data/repo

banner "Switch to Linux LTS"
sudo sed -i 's/^linux$/linux-lts/g' $PROFILEWORKDIR/packages.x86_64
sudo sed -i 's/linux/linux-lts/g' $PROFILEWORKDIR/airootfs/etc/mkinitcpio.d/linux.preset
# read -n1 -s -r -p $'Press space to continue...\n' key

banner "Make ISO $PROFILE"
sudo sed -i "2 a VERSION=$PROFILE" $PROFILEWORKDIR/profiledef.sh 
sudo ./mkarchiso.sh -v -w $WORKDIR -o $ISODIR $PROFILEWORKDIR
set -- $ISODIR/KavalirOS-$PROFILE-x86_64.iso
banner "$1 created"  

echo "Finalize Applications" >> /var/log/finalize-applications.log

# Remove Calamares icon form all desktops
if [ -f /home/$1/Desktop/calamares.desktop ]; then
    echo "Remove Calamares icon from desktop" >> /var/log/finalize-applications.log
    rm /home/$1/Desktop/calamares.desktop >> /var/log/finalize-applications.log
fi

#Enable kernel update
echo "Enable efi-update service" >> /var/log/finalize-applications.log
systemctl enable efi-update.path >> /var/log/finalize-applications.log

#Enable NTP
echo "Enable NTP service" >> /var/log/finalize-applications.log
timedatectl set-ntp true >> /var/log/finalize-applications.log

#Update user ID for same id as synaology nas
echo "Change id of user $1" >> /var/log/finalize-applications.log
usermod -u 1026 $1 >> /var/log/finalize-applications.log

# Boot EFI
echo "Copy boot files" >> /var/log/finalize-applications.log
cp /boot/vmlinuz-linux-lts /boot/efi/KavalirOS/ >> /var/log/finalize-applications.log
cp /boot/initramfs-linux-lts.img /boot/efi/KavalirOS/ >> /var/log/finalize-applications.log
cp /boot/amd-ucode.img /boot/efi/KavalirOS/ >> /var/log/finalize-applications.log

#Micro code
echo "Enable microcode in bootloader" >> /var/log/finalize-applications.log
sed -i '/^[[:blank:]]*#/d' /boot/efi/loader/entries/KavalirOS.conf >> /var/log/finalize-applications.log
sed -i '/^[[:space:]]*$/d' /boot/efi/loader/entries/KavalirOS.conf >> /var/log/finalize-applications.log
sed -i '2 a initrd /KavalirOS/amd-ucode.img' /boot/efi/loader/entries/KavalirOS.conf >> /var/log/finalize-applications.log

#NVidia
if [ -f /usr/bin/nvidia-xconfig ]; then
    echo "Apply NVidea settings" >> /var/log/finalize-applications.log
    /usr/bin/nvidia-xconfig >> /var/log/finalize-applications.log
fi

#Pacman
echo "Fix pacman config for repository on nas" >> /var/log/finalize-applications.log
sed -i 's/data\/repo/data\/nas\/roelof\/KavalirOS\/Repo/g' /etc/pacman.conf
sed -i  "$ a Defaults:$1 timestamp_timeout=30" /etc/sudoers

# Disable obsolete services
systemctl disable livecd-alsa-unmuter
systemctl disable reflector

#Create mirrorlist NL
reflector --protocol https --latest 10 --sort rate --country nl --save /etc/pacman.d/mirrorlist

#Disable autologin
sed -i 's/AutomaticLoginEnable=True/AutomaticLoginEnable=False/g' /etc/gdm/custom.conf
echo "" > /etc/motd

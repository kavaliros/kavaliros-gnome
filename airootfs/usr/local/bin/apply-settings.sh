echo "Apply application settings " >> /var/log/apply-settings.log

echo "Set default cursor to Bibata-Modern-Amber" >> /var/log/apply-settings.log
sed -i s/Adwaita/Bibata-Modern-Amber/g /usr/share/icons/default/index.theme >> /var/log/apply-settings.log

echo "Setup scanner" >> /var/log/apply-settings.log
if [ -f /usr/bin/brsaneconfig4 ]; then
    brsaneconfig4 -a name=Brother model=j4610dw ip=192.168.178.17 >> /var/log/apply-settings.log
    echo "Enable Cups service" >> /var/log/apply-settings.log
    systemctl enable cups
fi

# VirtualBox
if [ -f /usr/bin/virtualbox ]; then
    echo "Add user $1 to VBoxUsers group" >> /var/log/apply-settings.log
    gpasswd -a $1 vboxusers >> /var/log/apply-settings.log
fi

if [ -f /usr/lib/modules-load.d/virtualbox-guest-dkms.conf ]; then
    echo "Add user $1 to VBoxSf group" >> /var/log/apply-settings.log
    gpasswd -a $1 vboxsf >> /var/log/apply-settings.log
    echo "Enable VBoxService" >> /var/log/apply-settings.log
    systemctl enable vboxservice >> /var/log/apply-settings.log
fi

# VMware
if [ -f /etc/pam.d/vmtoolsd ]; then
    echo "Enable VMware shared folder" >> /var/log/apply-settings.log
    systemctl enable data-shared.mount >> /var/log/apply-settings.log
else
    echo "Disable VMware shared folder" >> /var/log/apply-settings.log
    systemctl disable data-shared.mount >> /var/log/apply-settings.log
    rm /etc/systemd/system/data-shared.mount >> /var/log/apply-settings.log
fi

# SyncThing
if [ -f usr/bin/syncthing ]; then
    echo "Enable Syncthing service" >> /var/log/apply-settings.log
    systemctl enable syncthing@$1 >> /var/log/apply-settings.log
fi

# NordVPN
if [ -f /usr/bin/nordvpn ]; then
    echo "Enable NordVPN service" >> /var/log/apply-settings.log
    systemctl enable nordvpnd >> /var/log/apply-settings.log
    echo "Add user $1 to NordVPN group" >> /var/log/apply-settings.log
    gpasswd -a $1 nordvpn >> /var/log/apply-settings.log
fi

# ICAclient. 
if [ -d /opt/Citrix/ICAClient ]; then
    echo "Apply ICAClient settings" >> /var/log/apply-settings.log
    tar xvf /etc/calamares/appsettings/ICAClient.tar.gz -C /home/$1 >> /var/log/apply-settings.log
fi

# NetExtender
if [ -f /usr/sbin/pppd ]; then
    echo "Apply NextExtender settings" >> /var/log/apply-settings.log
    chmod u+s /usr/sbin/pppd >> /var/log/apply-settings.log
    tar xvf /etc/calamares/appsettings/netextender.tar.gz -C /home/$1 >> /var/log/apply-settings.log
fi

#Displaylink
if [ -f /usr/lib/systemd/system/displaylink.service ]; then
    echo "Apply DisplayLink settings" >> /var/log/apply-settings.log
    systemctl enable displaylink >> /var/log/apply-settings.log
fi

#Dotnet
if [ -d /usr/share/dotnet ]; then
    echo "Apply .NET settings" >> /var/log/apply-settings.log
    dotnet tool install --global dotnet-dev-certs >> /var/log/apply-settings.log
    dotnet tool install --global dotnet-ef >> /var/log/apply-settings.log
    echo 'export PATH="$PATH:/home/username/.dotnet/tools"' | sudo tee /etc/profile.d/dotnet.sh 
    sed -i s/username/$1/g /etc/profile.d/dotnet.sh >> /var/log/apply-settings.log
    dotnet dev-certs https >> /var/log/apply-settings.log
fi

#Visual Studio Code OSS
if [ -f /usr/bin/code-oss ]; then
    echo "Apply Code-OSS settings" >> /var/log/apply-settings.log
    tar xvf /etc/calamares/appsettings/vscode-oss.tar.gz -C /home/$1 >> /var/log/apply-settings.log
    tar xvf /etc/calamares/appsettings/Code\ -\ OSS.tar.gz -C /home/$1/.config >> /var/log/apply-settings.log
fi

#Visual Studio Code 
if [ -f /opt/visual-studio-code/bin/code ]; then
    echo "Apply Visual Studio Code settings" >> /var/log/apply-settings.log
    tar xvf /etc/calamares/appsettings/vscode.tar.gz -C /home/$1 >> /var/log/apply-settings.log
    tar xvf /etc/calamares/appsettings/Code.tar.gz -C /home/$1/.config >> /var/log/apply-settings.log
fi

#Libre Office
if [ -f /usr/bin/libreoffice ]; then
    echo "Apply LibreOffice settings" >> /var/log/apply-settings.log
    tar xvf /etc/calamares/appsettings/libreoffice.tar.gz -C /home/$1/.config >> /var/log/apply-settings.log
fi

#Calibre
if [ -f /usr/bin/calibre ]; then
    echo "Apply Calibre settings" >> /var/log/apply-settings.log
    tar xvf /etc/calamares/appsettings/calibre.tar.gz -C /home/$1/.config >> /var/log/apply-settings.log
fi

#Gscan2Pdf
if [ -f /usr/bin/gscan2pdf ]; then
    echo "Apply GScan2PDF settings" >> /var/log/apply-settings.log
    tar xvf /etc/calamares/appsettings/gscan2pdfrc.tar.gz -C /home/$1/.config >> /var/log/apply-settings.log
fi

# Remove settings
echo "Remove all settings" >> /var/log/apply-settings.log
rm -Rf /etc/calamares/appsettings >> /var/log/apply-settings.log
